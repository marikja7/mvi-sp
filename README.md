# README

**Task name:** Text Clustering (2) <br/>
**Task:** For the dataset containing metadata from the ArXiv webpage https://www.kaggle.com/Cornell-University/arxiv create an efficient clustering model. <br/>
**Results:** 
- models for document embedding - Doc2Vec dbow, and Doc2Vec pvdm are stored in folder /models/embedding. 
- models for clustering are stored in /models/knn. <br/>
- results of clustering can be seen in Clustering notebook <br/>

**Future work:** evaluate embedding models based on document classification.


## Preprocessing.ipynb
Download dataset from https://www.kaggle.com/Cornell-University/arxiv. 
Create new dataset custom_dataset.csv with attributes:
- id 
- title
- abstract
- categories

Columns title and abstract are modified:
 - converting all words to lower case 
 - special characters removal
 - stop words removal
 - lemmatization
 
Example of a modified abstract can be seen in preprocessing.ipnb.

## Doc2Vec.ipynb
Two doc2vec models for document embedding are created
- dbow
- pvdm

these models are saved in folder ./embedding/models. Models are feed with data from custom_dataset.csv.

Embedding of titles from custom_dataset.csv with the use of both models is performed. 

## BERT.ipynb
Model istilbert-base-nli-mean-tokens is loaded and embedding is procced.

## Clustering.ipynb
Models for clustering are trained and saved in /models/knn. The result of clustering can be also seen in this file.

### Reference
https://arxiv.org/abs/1908.10084 <br/>
https://www.sbert.net/ <br/>
https://radimrehurek.com/gensim/models/doc2vec.html <br/>
https://medium.com/@ermolushka/text-clusterization-using-python-and-doc2vec-8c499668fa61 <br/>
https://towardsdatascience.com/document-embedding-techniques-fed3e7a6a25d <br/>
https://medium.com/dair-ai/tl-dr-sentencebert-8dec326daf4e <br/>
